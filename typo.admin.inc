<?php
/**
 * @file
 * Admin settings form for vk_openapi module
 */
function typo_admin_settings() {
  global $base_url;

  $form['typo_reports_ttl'] = array(
    '#type' => 'select',
    '#title' => t('Delete old typo reports'),
    '#description' => t('Delete typo reports older than N days'),
    '#default_value' => variable_get('typo_reports_ttl', 4320),
    '#options' => array(
      1440 => t('1 day'),
      4320 => t('3 days'),
      10080 => t('1 week'),
      0 => t('Never'),
    ),
  );

  return system_settings_form($form);
} 
(function ($) {
  // Show typo report window on Ctrl + Enter.
  // TODO: move this lines to behaviors.
  $(document).keydown(function(event) {
    if (event.ctrlKey && event.keyCode == 13) {
      typo_report_window();
    }
  });

  // callback for Drupal ajax_command_invoke function
  $.fn.typo_js_callback = function(res) {
    $('#typo-report-message').css({'display': 'none'});
    $('#typo-report-result').css({'display': 'block'}).html(Drupal.t('Your message has been sent. Thank you.'));
    console.log(res);
    setTimeout(modalContentClose, 1000);
  };

  /**
   * Function shows modal window.
   */
  function typo_report_window() {
    var sel = typo_get_sel_text();
    if (sel.selected_text.length > 20) {
      alert(Drupal.t('You have to select not more then 20 symbols'));
    }
    else if (sel.selected_text.length == 0) {
    }
    else {
      // Get selection context.
      var context = typo_get_sel_context(sel);

      // Show dialog.
      Drupal.CTools.Modal.show(Drupal.settings.TypoModal);
      $('#typo-modal-content').html('&nbsp;');
      $('#typo-report-content').appendTo('#typo-modal-content');
      
      $('#typo-context-div').html(context);
      $('#typo-context').val(context);
      $('#typo-url').val(window.location);
      
      // Close modal by Esc press.
      $(document).keydown(typo_close = function(e) {
        if (e.keyCode == 27) {
          modalContentClose();
          $(document).unbind('keydown', typo_close);
        }
      });

      // Close modal by clicking outside the window.
      $('#modalBackdrop').click(typo_click_close = function(e) {
        modalContentClose();
        $('#modalBackdrop').unbind('click', typo_click_close);
      });
    }
  }
})(jQuery);
(function ($) {
  /**
   * Provide the HTML to create the modal dialog.
   * Clone of function Drupal.theme.prototype.CToolsModalDialog.
   */
  Drupal.theme.prototype.TypoModalDialog = function () {
    var close_image = '';
    if (Drupal.settings.TypoModal.closeImage) {
      close_image = Drupal.settings.TypoModal.closeImage;
    }

    var html = ''
    html += '  <div id="ctools-modal">'
    html += '    <div id="typo-modal">'
    html += '      <div class="ctools-modal-content">' // panels-modal-content
    html += '        <div class="modal-header">';
    html += '          <a id="close" href="#">';
    html +=              Drupal.settings.TypoModal.closeText + close_image;
    html += '          </a>';
    html += '          <span id="modal-title" class="modal-title">&nbsp;</span>';
    html += '        </div>';
    html += '        <div id="typo-modal-content" class="modal-content">';
    html += '        </div>';
    html += '      </div>';
    html += '    </div>';
    html += '  </div>';

    return html;
  }
})(jQuery);
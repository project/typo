<div id="typo-report-content">
  <div id="typo-report-message">
    <div id="typo-message">
<?php
      print t('You think, that the text:');
?>
      <div id="typo-context-div"></div>
<?php
      print t('contains typo? Press button "Submit typo report". Also you can attach your comment to this report.');
?>
    </div>
    <div id="typo-form">
<?php
    print drupal_render(drupal_get_form('typo_report_form'));
?>
    </div>
  </div>
  <div id="typo-report-result">
  </div>
</div>
<div id="tmp"></div>